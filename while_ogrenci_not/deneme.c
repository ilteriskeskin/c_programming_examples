/*
 * kullanıcı negatif değerse döngü sonlanır
 */

#include <stdio.h>

int main(){
	int puan;
	int sayac = 1;
	int toplam = 0;
	float ortalama;

	printf("Ogretmen ogrenci notlarini girer ve negatif değer girilince not girilmeyi keser.\n\n");

	while( puan > 0){
		printf("%d. Ogrencinin notunu giriniz: ", sayac);
		scanf("%d", &puan);
		
		if (puan >= 0 && puan <= 100){
			toplam += puan;
			sayac++;
		}
		else if (puan > 100){
			printf("Ogrencinin not degeri 0-100 arasinda olmalidir.\n");
			printf("Lutfen yeniden giriniz.\n");
		}
	}

	ortalama = (float)toplam / (sayac - 1);
	printf("Toplam ogrenci sayisi= %d \n", (sayac-1));
	printf("Ortalama not= %.2f \n", ortalama);

	return 0;
}
